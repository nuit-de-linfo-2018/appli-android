package com.goats.nuitdelinfo2018_goats;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.EventLog;
import android.util.Log;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class droneActivity extends AppCompatActivity implements SensorEventListener{

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private TextView XData, YData, ZData;
    private ToggleButton crashSensors, wallCrash;

    private float deltaX = 0;
    private float deltaY = 0;
    private float deltaZ = 0;

    private float lastX, lastY, lastZ;

    private SensorEvent myEvent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drone);
        setTitle("Drone");
        initTextViews();
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
        {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
        else
        {
            Log.e("Accelerometter Error","NO ACCELEROMETTER");
        }
        trustAllCertificates();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        sensorManager.registerListener(this, accelerometer, 5000000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        myEvent = event;

        new Thread() {
            public void run() {
                        JSONObject json = new JSONObject();

                        deltaX = Math.abs(lastX - myEvent.values[0]);
                        deltaY = Math.abs(lastY - myEvent.values[1]);
                        deltaZ = Math.abs(lastZ - myEvent.values[2]);
                        if(crashSensors.isChecked())
                        {
                            deltaX = 0; deltaY = 0; deltaZ = 0;
                        }
                        if(wallCrash.isChecked())
                        {
                            deltaX = Float.parseFloat("0.2"); deltaY = Float.parseFloat("20.5"); deltaZ = Float.parseFloat("9.7");
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                XData.setText(String.valueOf(deltaX));
                                YData.setText(String.valueOf(deltaY));
                                ZData.setText(String.valueOf(deltaZ));
                            }
                        });

                        JSONObject dataJson = new JSONObject();
                        try {
                            json.put("name", "Drone 212");
                            json.put("data type", "accelerometter");
                            dataJson.put("XData", String.valueOf(deltaX));
                            dataJson.put("YData", String.valueOf(deltaY));
                            dataJson.put("ZData", String.valueOf(deltaZ));
                            json.put("data", dataJson);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Uri.Builder builder = new Uri.Builder();
                        builder.scheme("https")
                                .encodedAuthority("goat.agravelot.eu")
                                .appendPath("api")
                                .appendPath("data")
                                .appendQueryParameter("type", "accelerometer")
                                .appendQueryParameter("device", "Drone")
                                .appendQueryParameter("name", "Drony McDroneface")
                                .appendQueryParameter("value", json.toString());
                        try {
                            URL url = new URL(builder.build().toString());
                            URLConnection con = url.openConnection();
                            HttpURLConnection http = (HttpURLConnection)con;
                            http.setRequestMethod("POST");
                            http.setDoOutput(true);
                            Log.e("url", url.toString());

                            OutputStream os = http.getOutputStream();
                            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                            writer.flush();
                            writer.close();
                            os.close();

                            int responseCode=http.getResponseCode();

                            if (responseCode == HttpsURLConnection.HTTP_OK) {

                                BufferedReader in=new BufferedReader(new
                                        InputStreamReader(
                                        http.getInputStream()));

                                StringBuffer sb = new StringBuffer("");
                                String line="";

                                while((line = in.readLine()) != null) {

                                    sb.append(line);
                                    break;
                                }

                                in.close();
                                Log.v("jsp",sb.toString());

                            }
                            else {
                                Log.v("jsp", String.valueOf(responseCode));
                            }
                        } catch (UnsupportedEncodingException e1) {
                            e1.printStackTrace();
                        } catch (ProtocolException e1) {
                            e1.printStackTrace();
                        } catch (MalformedURLException e1) {
                            e1.printStackTrace();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        Log.v("Json", json.toString());
                        //throw new NullPointerException();
                    }
        }.start();



    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void initTextViews()
    {
        XData = findViewById(R.id.XData);
        YData = findViewById(R.id.YData);
        ZData = findViewById(R.id.ZData);
        crashSensors = findViewById(R.id.crashSensorButton);
        wallCrash = findViewById(R.id.wallCrashSimulation);
    }

    public void trustAllCertificates() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                            return myTrustedAnchors;
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception e) {
        }
    }
}
