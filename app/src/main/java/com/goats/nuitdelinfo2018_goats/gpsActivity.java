package com.goats.nuitdelinfo2018_goats;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class gpsActivity extends AppCompatActivity {

    private TextView positionTV;
    private Location myLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);
        setTitle("GPS");
        positionTV = findViewById(R.id.positionTV);
        trustAllCertificates();
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                myLocation = location;
                positionTV.setText(String.valueOf(myLocation.getLatitude()) + " - " + String.valueOf(myLocation.getLongitude()));
                Log.e("position", myLocation.toString());

                new Thread() {
                    public void run() {
                        JSONObject dataJson = new JSONObject();
                        try {
                            dataJson.put("latitude", String.valueOf(myLocation.getLatitude()));
                            dataJson.put("longitude", String.valueOf(myLocation.getLongitude()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Uri.Builder builder = new Uri.Builder();
                        builder.scheme("https")
                                .encodedAuthority("goat.agravelot.eu")
                                .appendPath("api")
                                .appendPath("data")
                                .appendQueryParameter("type", "GPS")
                                .appendQueryParameter("device", "GPS Beacon 12")
                                .appendQueryParameter("name", "Lord McGeperdu")
                                .appendQueryParameter("value", dataJson.toString());
                        try {
                            URL url = new URL(builder.build().toString());
                            URLConnection con = url.openConnection();
                            HttpURLConnection http = (HttpURLConnection)con;
                            http.setRequestMethod("POST");
                            http.setDoOutput(true);
                            Log.e("url", url.toString());

                            OutputStream os = http.getOutputStream();
                            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                            writer.flush();
                            writer.close();
                            os.close();

                            int responseCode=http.getResponseCode();

                            if (responseCode == HttpsURLConnection.HTTP_OK) {

                                BufferedReader in=new BufferedReader(new
                                        InputStreamReader(
                                        http.getInputStream()));

                                StringBuffer sb = new StringBuffer("");
                                String line="";

                                while((line = in.readLine()) != null) {

                                    sb.append(line);
                                    break;
                                }

                                in.close();
                                Log.v("jsp",sb.toString());

                            }
                            else {
                                Log.v("jsp", String.valueOf(responseCode));
                            }
                        } catch (UnsupportedEncodingException e1) {
                            e1.printStackTrace();
                        } catch (ProtocolException e1) {
                            e1.printStackTrace();
                        } catch (MalformedURLException e1) {
                            e1.printStackTrace();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        Log.v("Json", dataJson.toString());
                        //throw new NullPointerException();
                    }

                }.start();


            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

// Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    public void trustAllCertificates() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                            return myTrustedAnchors;
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception e) {
        }
    }
}
