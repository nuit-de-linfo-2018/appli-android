package com.goats.nuitdelinfo2018_goats;

import android.Manifest;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void homeClickListener(View view)
    {
        switch (view.getId())
        {
            case R.id.droneButton :
                startDroneActivity();
                break;
            case R.id.gpsButton :
                startGPSActivity();
                break;
            case R.id.panneauSolaireButton :
                startSolarPannelActivity();
                break;
            case R.id.eolienneButton :
                startEolienneActivity();
                break;
            default :
        }
    }

    private void startDroneActivity()
    {
        this.startActivity( new Intent(this, droneActivity.class));
    }

    private void startSolarPannelActivity()
    {
        this.startActivity( new Intent(this, solarPannelActivity.class));
    }

    private void startGPSActivity()
    {
        this.startActivity( new Intent(this, gpsActivity.class));
    }

    private void startEolienneActivity()
    {
        this.startActivity( new Intent(this, eolienneActivity.class));
    }

}
